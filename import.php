<?php
  function get_traffic24($town="Cape Town")
  {
  $town = str_replace(" ","%20",$town);
      $url = "http://wsmobile.24.com/TrafficIncident.svc/Town/".$town."?pageNo=0&pageSize=100";
    //echo "url:".$url;
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $auth = curl_exec($curl);
      //echo'auth:'. $auth;
    if($auth)
    {
      $json = json_decode($auth); 
      //echo '<pre>'.print_r($json,true).'</pre>';
      
      foreach($json->Entities as $report){
      
        $id  = $report->Id;
        $area = $report->Area;
        $description = $report->Description;
        $title = $report->Road;
        $city = $town;
      if(!empty($report->BeginLoc->lat)){
          $lat = $report->BeginLoc->lat+ mt_rand() / mt_getrandmax() / 1000;
          $long = $report->BeginLoc->lon+ mt_rand() / mt_getrandmax() / 1000;
        }else{
          //if the lat is empty then try and get the lat and long with the coords function      
          $coords = getCoordinates($town);

          $lat = $coords['lat'] + mt_rand() / mt_getrandmax() / 1000;
          $long = $coords['long'] + mt_rand() / mt_getrandmax() / 1000;
          
        }
        
        $data_array = array("user_id"=>$id, "first_name" => "News", "last_name" => "24", "tel" => "","email" => "","appear_anon" => 0, "title" => $title,"description" => $description, "longitude"=> $long, "latitude"=>$lat, "width"=> 400, "height"=> 300, "upload_date"=> time(),"image_url"=>"images/24.jpeg", "url"=>"","address"=>$town,"category"=>"Traffic","vote"=>0);
        
        $jsonString = file_get_contents('results.json');
        if($jsonString != "null"){
          $data = json_decode($jsonString,true);
        }else{
          $data = array();
        }
        array_push($data,$data_array);
        file_put_contents('results.json', json_encode($data));
      }
    }
    //echo "Success";
  }
  
//get coordinates
function getCoordinates($address){
$address = str_replace(" ", "+", $address);
$url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
$response = file_get_contents($url);
$json = json_decode($response,TRUE);

return array('lat'=>$json['results'][0]['geometry']['location']['lat'],"long"=>$json['results'][0]['geometry']['location']['lng']);
}


get_traffic24();
get_traffic24("Durban");
get_traffic24("Johannesburg");
get_traffic24("Port Elizabeth");
get_traffic24("Bloemfontein");
$urltg = str_replace('form.html',"index.php",$_SERVER['HTTP_REFERER']);
//$urltg = $urltg."?import=true";
$_SESSION['import'] = true;
//echo '<pre>'.print_r($urltg,true).'</pre>';
header("Location:$urltg");

?>