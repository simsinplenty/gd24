<?php 
  //echo '<pre>'.print_r($_POST,true).'</pre>'; 
  //echo '<pre>'.print_r($_FILES,true).'</pre>'; 
////////////////////upload////////////////////////////////
$target_dir = "images/";
$target_file = $target_dir . basename($_FILES["news_file"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["news_file"]["tmp_name"]);
    if($check !== false) {
        //echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        //echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["news_file"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["news_file"]["tmp_name"], $target_file)) {
       // echo "The file ". basename( $_FILES["news_file"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
//////////////////////////////////////////upload/////////////////////////////////////////

$lat = 0;
$long = 0;
$address = $_POST['news_City']." ".$_POST['news_street'];
$coords = getCoordinates($address);
$lat = $coords['lat'];
$long = $coords['long'];
//echo'coords:'. $lat.','.$long;
$jsonString = file_get_contents('results.json');
//echo '<pre>'.print_r($jsonString,true).'</pre>';
if($jsonString != "null"){
  $data = json_decode($jsonString,true);
}else{
  $data = array();
}
$id = time();
$data_array = array("user_id"=>$id, "first_name" => $_POST['news_name_first'], "last_name" => $_POST['news_name_last'], "tel" => $_POST['news_tel'],"email" => $_POST['news_email'],"appear_anon" => $_POST['news_anon'], "title" => $_POST['news_title'],"description" => $_POST['news_description'], "longitude"=> $long, "latitude"=>$lat, "width"=> 500, "height"=> 375, "upload_date"=> time(),"image_url"=>$target_file, "url"=>$_POST['news_url'],"address"=>$address,"category"=>$_POST['news_type'],"vote"=>0);
array_push($data,$data_array);

file_put_contents('results.json', json_encode($data));
//echo '<pre>'.print_r($data,true).'</pre>';

//echo "address:".$address;
?>

<?php
//get coordinates
function getCoordinates($address){
$address = str_replace(" ", "+", $address);
$url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
$response = file_get_contents($url);
$json = json_decode($response,TRUE);

return array('lat'=>$json['results'][0]['geometry']['location']['lat'],"long"=>$json['results'][0]['geometry']['location']['lng']);
}


//echo 'get_traffic24: '.get_traffic24();
//echo getCoordinates($address);

// Function to get the client IP address
function get_ip() {
		return  $_SERVER['REMOTE_ADDR'];
	}

//echo'ip:'. get_ip();
$urltg = str_replace('form.html',"index.php",$_SERVER['HTTP_REFERER']);
$urltg = $urltg."?id=".$id;
//echo '<pre>'.print_r($urltg,true).'</pre>';
header("Location:$urltg");
die();
?>
<!--
<object width="425" height="350" data="http://www.youtube.com/v/Ahg6qcgoay4" type="application/x-shockwave-flash"><param name="src" value="http://www.youtube.com/v/Ahg6qcgoay4" /></object>
-->