<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Your voice</title>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script><?php
        $jsonString = file_get_contents('results.json');
        echo 'var data = {"count":3,"photos":'.$jsonString.'};';
        if(isset($_GET['id']))
        echo 'var upload_id ='.$_GET['id'].';';
        //echo 'alert(upload_id);';
      ?>
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <style>
      body {
        margin: 0;
        padding: 0;
        font-family: Arial;
        font-size: 14px;
      }
      #panel {
        float: left;
        width: 300px;
        height: 550px;
      }
      #map-container {
        margin-left: 300px;
      }
      #map {
        width: 100%;
        height: 550px;
      }
      #markerlist {
        /*height: 400px;*/
        margin: 10px 5px 0 10px;
        overflow: auto;
      }
      .title {
        border-bottom: 1px solid #e0ecff;
        overflow: hidden;
        width: 256px;
        cursor: pointer;
        padding: 2px 0;
        display: block;
        color: #000;
        text-decoration: none;
      }
      .title:visited {
        color: #000;
      }
      .title:hover {
        background: #e0ecff;
      }
      #timetaken {
        color: #f00;
      }
      .info {
        width: 200px;
      }
      .info img {
        border: 0;
      }
      .info-body {
        width: 200px;
       
        line-height: 200px;
        margin: 2px 0;
        text-align: center;
        overflow: hidden;
      }
      .info-img {
        height: 220px;
        width: 200px;
      }

      .navbar-default .navbar-nav > li > option:focus, .navbar-default .navbar-nav > li > option:hover {
        color: #333;
        background-color: transparent;
      }
      .navbar-default .navbar-nav > li > option {
        color: #777;
      }
      .nav > li > option:focus, .nav > li > option:hover {
        text-decoration: none;
        background-color: #EEE;
      }
      .navbar-nav > li > option {
        line-height: 20px;
      }

      .nav > li > option {
        position: relative;
        display: block;
        padding: 10px 15px;
        padding-top: 15px;
        padding-bottom: 15px;
      }
      option:focus {
        outline: thin dotted;
        outline-offset: -2px;
      }
      option {
        text-decoration: none;
        background-color: transparent;
      }

    </style>


    <script>

      var script = '<script type="text/javascript" src="src/markerclusterer';
      if (document.location.search.indexOf('compiled') !== -1) {
        script += '_compiled';
      }
      script += '.js"><' + '/script>';
      document.write(script);
    </script>
    <script src="speed_test.js"></script>

    <script>
      google.maps.event.addDomListener(window, 'load', speedTest.init);
    </script>
    <script>
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-12846745-20']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>
  </head>

  <body>
    <div id="panel" class="text-center">
      <a href=# ><img src="images/yourvoiceheader.png" style="margin-bottom: 5%" ></a>
     <!-- <p>
        <a class="btn btn-default" href="?compiled">Compiled</a> |
        <a class="btn btn-default" href="?">Standard</a>
      </p>-->

      <div>
        <input type="checkbox" class="checkbox-inline" checked="checked" id="usegmm"/>
        <span>Cluster</span>
      </div>
      <br>

      <select id="categories" class="btn btn-default dropdown-toggle" style="width:256px;">
          <ul class="nav navbar-nav">
            <li>
              <option value="Corruption" href="#" >Corruption</option>
            </li>
            <li>
              <option href="#" value="Crime">Crime</option>
            </li>
            <li>
              <option href="#" value="Good News" selected="selected">Good News</option>
            </li>
            <li>
              <option  href="#" value="News24">News24</option>
            </li>
            <li>
              <option href="#" value="Protests">Protests</option>
            </li>
            <li >
              <option  href="#" value="Service Delivery">Service Delivery</option>
            </li>
            <li>
              <option  href="#" value="Traffic">Traffic</option>
            </li>
          </ul>
      </select>

    <div style="visibility: hidden">
        Markers:
        <select id="nummarkers" >
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3" selected="selected">3</option>
          <option value="4">4</option>

        </select>
  <span>Time used: <span id="timetaken"></span> ms</span>
      </div>
      <strong>CATEGORY</strong>

      <div class="mCustomScrollbar" data-mcs-theme="dark">
      <div id="markerlist" class="list-group-item" >

        </div>

      </div>
    </div>
    <div id="map-container">
      <div id="map"></div>
    </div>
    <div style = "margin-top:100px;"><a class="btn btn-default"  href = "form.html">Upload</a>
    <a class="btn btn-default"  href = "import.php">Import</a>
    </div>    
  </body>
</html>
